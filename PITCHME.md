

## Markdown Presentation

![Simplicity is the Ultimate Sophistication](https://tedcurran.net/wp-content/uploads/2017/10/da-vinci-markdown-presentation.gif)

by [@TedCurran](https://twitter.com/TedCurran) 


<!-- writing a presentation in markdown, updating in Git --!


---

![Leonardo Da Vinci, CC Wikimedia Commons](https://i.imgur.com/XUqVnU9.png)



> *“Simplicity is the ultimate sophistication.”* 
> — Leonardo da Vinci







---



## Authoring in Markdown, Publishing with Git

This presentation was written entirely in [Markdown](http://daringfireball.net/projects/markdown) format, sync'd to [our internal Autodesk Enterprise Bitbucket account](https://git.autodesk.com) and published to [GitPitch](https://gitpitch.com/), which automatically converts markdown files to HTML5 presentations using the [Reveal.js](http://lab.hakim.se/reveal-js/) platform. 



---



### .MD — Why?

--- 

#### It's FAST to write. 

Write in text, don't worry about visuals. It's styled automatically.

---

#### It's FAST to read (on any device). 

- Responsive HTML5 design is mobile-friendly by default.
- Touch-sensitive -- swipe to advance slides.

---

#### Great for adapting PowerPoints for web delivery

- best for communicating bullets and images
- not designed for interactivity -- do that in Evo



---

#### Git provides version control, collaboration, security

- every update is logged for more efficient localization of updates


---


The source file looks like this: 

```md
A strength of this approach is the simplicity of authoring content 

* Markdown is a plain-text format, editable in any text editor
* It compiles to valid HTML that works in any modern browser
* It's easy to localize since it does not require specialized software to access 
```

---

## Media

It's not recommended to host rich media in the Git repo, so it's best to embed media hosted on other cloud locations like our [Akamai](https://www.akamai.com/) or [Ooyala](http://www.ooyala.com), or on public clouds like [YouTube](https://www.youtube.com/), [UnSplash](https://unsplash.com), [OneDrive](https://onedrive.com), and others. 


---

## Save Offline

Presentations can be downloaded as PDF or HTML for offline use. 

---

## Alternate Publishing



I have also successfully published this same presentation using Deckdown. Like GitPitch, it goes to a publicly available GItHub or Bitbucket URL, pulls in a Markdown file, and beautifies it with Reveal. 



This same deck, when accessed via Deckdown, is [available here ↗](http://deckdown.org/deck?src=https://bitbucket.org/tedcurran/prezos/raw/78045d356c46e5323abb10de74860ec221213751/PITCHME.md#/)



---

