# README #

This repository is a proof-of-concept for building HTML presentations in Markdown and managing the file versions using Git. 

The goal of this project is to explore alternative methods of building learning content in a more streamlined, web-friendly, and future-proof way. 



![Simplicity is the Ultimate Sophistication](https://tedcurran.net/wp-content/uploads/2017/10/da-vinci-markdown-presentation.gif)



### What is this repository for? ###

* rapidly mocking up HTML5 presentations
* demonstrating the power of Git version control for tracking changes in file assets
* showing an alternative approach for text-based lesson design
* Testing rapid Markdown-to-presentation tools like [GitPitch](https://gitpitch.com/tedcurran/prezos/master?grs=bitbucket&t=white#/) or [Deckdown](http://deckdown.org/deck?src=https://bitbucket.org/tedcurran/prezos/raw/f2a2e74fbbdadf82ef043ea72862da55b99f9bd1/PITCHME.md). 




### How can I contribute?

- Please feel free to [fork this repo](https://bitbucket.org/tedcurran/prezos/overview) (or start one of your own). 
- [Familiarize yourself with Markdown](https://guides.github.com/features/mastering-markdown/) and create your own presentation!
- Explore a great markdown editor like [Typora](https://typora.io/). 



### Who do I talk to? ###

* [Ted Curran](mailto:ted.curran@autodesk) in Sales Learning & Technology



---



## Watch what happens when I update a file in this repository...

[saving]